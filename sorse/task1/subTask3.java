package saha.study.java.sorse.task1;

public class subTask3 {
    public static void main(String[] args) {
        int a3 = Integer.parseInt(args[0]);
        int b3 = Integer.parseInt(args[1]);
        int count = Math.min(a3, b3);
        for (int i = count; i >= 1; i--) {
            if (a3 % i == 0 && b3 % i == 0) {
                count = i;
                break;
            }
        }
        System.out.println(count);
        System.out.println();
    }
}
