package saha.study.java.sorse.task1;

public class subTask6 {
    public static void main(String[] args) {

        String a = args[0];
        System.out.println(strToNum(a));

         String b = args[1];
        int n = Integer.parseInt(b);
        System.out.println(convertNumToStr(n));

    }

    public static int strToNum(String number) {
        int res = 0;
        number = number.toUpperCase( );
        for (int i = 0, j = number.length() - 1; i < number.length(); i++, j--) {
            res += (number.charAt(j) - 64) * Math.pow(26, i);
        }
        return res;
    }

    public static String convertNumToStr(int number) {

        StringBuilder result = new StringBuilder();
        int delimoe = number;
        if (number < 27) {
            result.append(numToStr(number));
            return result.reverse().toString();
        }
        while (true) {
            int ostatok = delimoe % 26;
            result.append(numToStr(ostatok));
            delimoe = delimoe / 26;

            if (ostatok == 0) {
                delimoe = delimoe - 1;
            }

            if (delimoe < 26) {
                result.append(numToStr(delimoe));
                break;
            }

        }
        return result.reverse().toString();
    }

    private static String numToStr(int a) {
        switch (a) {
            case 1:
                return "A";
            case 2:
                return "B";
            case 3:
                return "C";
            case 4:
                return "D";
            case 5:
                return "E";
            case 6:
                return "F";
            case 7:
                return "G";
            case 8:
                return "H";
            case 9:
                return "I";
            case 10:
                return "J";
            case 11:
                return "K";
            case 12:
                return "L";
            case 13:
                return "M";
            case 14:
                return "N";
            case 15:
                return "O";
            case 16:
                return "P";
            case 17:
                return "Q";
            case 18:
                return "R";
            case 19:
                return "S";
            case 20:
                return "T";
            case 21:
                return "U";
            case 22:
                return "V";
            case 23:
                return "W";
            case 24:
                return "X";
            case 25:
                return "Y";
            case 26:
                return "Z";
            case 0:
                return "Z";
            default:
                return "";
        }
    }
}