package saha.study.java.sorse.task1;

import java.util.Scanner;

public class demoTask1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("subTask1");
        System.out.println("Сума двох дiйсних чисел");
        subTask1.main(new String[]{"5.3223", "6"});

        System.out.println("subTask2");

        String a2 = sc.nextLine();
        String b2 = sc.nextLine();
        subTask2.main(new String[]{a2, b2});

        System.out.println("subTask3");
        System.out.println("Найбiльший спiльний дiльник");
        subTask3.main(new String[] {"20", "100"});

        System.out.println("subTask4");
        System.out.println("Сума окремих розрядiв цiлого числа.");
        subTask4.main(new String[] {"567892344"});

        System.out.println("subTask5");
        System.out.println("Простi числа в дiапазонi вiд 1 до n");
        String a5 = sc.nextLine();
        subTask5.main(new String[]{a5});

        System.out.println("subTask6");
        System.out.println("Метод визначення порядкового номера стовпця за його буквеним номером");
        String a6 = sc.nextLine();
        System.out.println("Метод визначення літери стовпця за його порядковим номером");
        String b6 = sc.nextLine();
        System.out.println("Метод визначення номера стовпця, який розміщено праворуч від заданого.");
        String c6 = sc.nextLine();
        subTask6.main(new String[]{a6,b6,c6});


    }
}
